import { build } from 'vite';
import createVuePlugin from '@vitejs/plugin-vue';

(async () => {
    await build({
        root: './src',
        build: {
            outDir: '../dist'
        },
        plugins: [
            createVuePlugin()
        ],
        define: {
            'BUILD_TIMESTAMP': Date.now()
        }
    });
})().catch(console.error);