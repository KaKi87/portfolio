export default [
    {
        orgId: 'iut-unilim',
        id: 'lp',
        startDate: '2019',
        endDate: '2020',
        label: {
            'en': `Bachelor\xA0: web\xA0development`,
            'fr': `Licence Professionnelle\xA0: développement\xA0web`
        }
    },
    {
        orgId: 'beaupeyrat',
        id: 'bts',
        startDate: '2017',
        endDate: '2019',
        label: {
            'en': `BTEC Higher National Diploma\xA0: web\xA0development`,
            'fr': `Brevet de Technicien Supérieur\xA0: développement\xA0web`
        }
    },
    {
        orgId: 'turgot',
        id: 'bac',
        startDate: '2016',
        endDate: '2017',
        label: {
            'en': `BTEC National Diploma`,
            'fr': `Baccalauréat technologique`
        }
    }
];