export const
    workExperience = [
        {
            orgId: 'friendlyapps',
            id: 'friendlyapps',
            startDate: '2024',
            title: {
                'en': `In the process of creating a web agency`,
                'fr': `En cours de création d'une agence web`
            }
        },
        {
            orgId: 'goomeo',
            id: 'goomeo-full-stack-web-developer',
            startDate: '2020-08-01',
            endDate: '2023-01-23',
            title: {
                'en': `Full-stack web developer`,
                'fr': `Développeur web full-stack`
            },
            description: {
                'en': `Creation of a videoconferencing application incorporating optimized tools for sales teams.`,
                'fr': `Création d'une application de visioconférence intégrant des outils optimisés pour les équipes commerciales.`
            }
        },
        {
            orgId: 'goomeo',
            id: 'goomeo-apprentice',
            startDate: '2019-09-02',
            endDate: '2020-07-31',
            title: {
                'en': `Apprentice`,
                'fr': `Apprenti`
            },
            description: {
                'en': `Creation of an app offering real-time interactivity (polls, chat, streaming) on events (exhibitions, conventions).`,
                'fr': `Création d'une application proposant de l'interactivité en temps réel (sondages, chat, streaming) dans les événements (salons, congrès).`
            }
        },
        {
            orgId: 'atr-limousin',
            id: 'atr-limousin-intern',
            startDate: '2018-12-03',
            endDate: '2019-01-25',
            title: {
                'en': `Intern`,
                'fr': `Stagiaire`
            }
        },
        {
            orgId: 'orange',
            id: 'orange-intern',
            orgOfficeId: 'drtc',
            startDate: '2018-05-22',
            endDate: '2018-07-13',
            title: {
                'en': `Intern`,
                'fr': `Stagiaire`
            }
        }
    ],
    volunteerExperience = [
        {
            orgId: 'alternatives-87',
            startDate: '2022-06-25',
            title: {
                'en': `Vice-president`,
                'fr': `Vice-président`
            }
        },
        {
            orgId: 'vision-limousin',
            startDate: '2021-02-24',
            endDate: '2023-02-26',
            title: {
                'en': `Member`,
                'fr': `Membre`
            },
            isVisibleInResume: false
        },
        {
            orgId: 'adaes',
            startDate: '2020-09-26',
            title: {
                'en': `Member`,
                'fr': `Membre`
            },
            description: {
                'en': `A community for sharing knowledge and practice astronomy.`,
                'fr': `Communauté de partage de connaissances et de pratique de l'astronomie.`
            }
        },
        {
            orgId: 'vision-limousin',
            startDate: '2020-02-26',
            endDate: '2021-02-24',
            title: {
                'en': `Treasurer`,
                'fr': `Trésorier`
            }
        },
        {
            orgId: 'limouzicodev',
            startDate: '2019-03-20',
            title: {
                'en': 'Member',
                'fr': 'Membre'
            },
            description: {
                'en': `Software & web development workshops at Ester Technopole.`,
                'fr': `Animation d'ateliers de développement logiciel & web à Ester Technopole.`
            }
        },
        {
            orgId: 'vision-limousin',
            startDate: '2019-02-18',
            endDate: '2020-02-26',
            title: {
                'en': `Secretary`,
                'fr': `Secrétaire`
            },
            description: {
                'en': `Cultural promotion of our region through multimedia & photography.`,
                'fr': `Promotion culturelle de la région ex-Limousin à travers le multimédia et la photographie.`
            }
        },
        {
            orgId: 'alternatives-87',
            startDate: '2018-03-02',
            endDate: '2022-06-25',
            title: {
                'en': `Member`,
                'fr': `Membre`
            },
            description: {
                'en': `Promotion of free software through educational & fun workshops in the area.`,
                'fr': `Promotion du logiciel libre par l'animation d'ateliers éducatifs et ludiques dans le département.`
            }
        },
        {
            orgId: 'oreplains',
            startDate: '2016-12-15',
            endDate: '2018-08-25',
            title: {
                'en': `Technician`,
                'fr': `Technicien`
            },
            description: {
                'en': `Linux system administration for web & game servers.`,
                'fr': `Administration système sous Linux de serveurs web et de jeu.`
            },
            isVisibleInResume: false
        }
    ];