export default [
    {
        id: 'science',
        label: {
            'en': `Science`,
            'fr': `Sciences`
        },
        items: [
            {
                id: 'computing',
                label: {
                    'en': `Computing`,
                    'fr': `Informatique`
                }
            },
            {
                id: 'astronomy',
                label: {
                    'en': `Astronomy`,
                    'fr': `Astronomie`
                }
            },
            {
                id: 'more',
                label: {
                    'en': `& more`,
                    'fr': `et + encore`
                }
            }
        ]
    },
    {
        id: 'cinema',
        label: {
            'en': `Cinema`,
            'fr': `Cinéma`
        },
        items: [
            {
                id: 'sci-fi',
                label: {
                    'en': `Sci-fi movies & TV`,
                    'fr': `Films & séries de science-fiction`
                }
            },
            {
                id: 'pop-science',
                label: {
                    'en': `Popular science on YouTube`,
                    'fr': `Vulgarisation scientifique sur YouTube`
                }
            }
        ]
    },
    {
        id: 'nature',
        label: {
            'en': `Nature`
        }
    },
    {
        id: 'photography',
        label: {
            'en': `Photography`,
            'fr': `Photographie`
        }
    }
];