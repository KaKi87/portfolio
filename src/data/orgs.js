export default [
    {
        id: 'friendlyapps',
        name: 'FriendlyApps',
        location: 'Limoges',
        type: 'company'
    },
    {
        id: 'adaes',
        name: 'ADAES',
        location: 'Limoges',
        type: 'nonprofit',
        thumbnailUrl: '/thumbnail_adaes.webp',
        url: 'https://adaeslimoges.fr/association/'
    },
    {
        id: 'goomeo',
        name: `Goomeo`,
        location: 'Limoges',
        type: 'company',
        thumbnailUrl: '/thumbnail_goomeo.jpg',
        additionalSkills: [
            {
                categoryId: 'others',
                id: 'sysadmin'
            }
        ]
    },
    {
        id: 'limouzicodev',
        name: 'LimouziCoDev',
        location: 'Limoges',
        type: 'nonprofit',
        thumbnailUrl: '/thumbnail_limouzicodev.webp',
        url: 'https://limouzico.dev'
    },
    {
        id: 'vision-limousin',
        name: `Vision Limousin`,
        location: 'Limoges',
        type: 'nonprofit',
        thumbnailUrl: '/thumbnail_vision-limousin.svg',
        url: 'https://www.instagram.com/vision_limousin'
    },
    {
        id: 'iut-unilim',
        name: 'IUT du Limousin',
        location: 'Limoges',
        type: 'education',
        thumbnailUrl: '/thumbnail_iut-unilim.png',
        url: 'https://www.iut.unilim.fr/wp-content/uploads/sites/3/2021/11/Fiche-LP-INFO-DAWBG.pdf'
    },
    {
        id: 'atr-limousin',
        name: `ATR Limousin`,
        location: 'Limoges',
        type: 'company',
        thumbnailUrl: '/thumbnail_atr-limousin.png',
        url: 'https://www.atr-limousin.fr/'
    },
    {
        id: 'orange',
        name: `Orange`,
        offices: {
            'drtc': {
                name: {
                    'en': `PSTN directorate`,
                    'fr': `Direction du Réseau Téléphonique Commuté`
                },
                location: 'Lyon'
            }
        },
        type: 'company',
        thumbnailUrl: '/thumbnail_orange.svg'
    },
    {
        id: 'alternatives-87',
        name: `Alternatives 87`,
        location: 'Limoges',
        type: 'nonprofit',
        thumbnailUrl: '/thumbnail_alternatives-87.png'
    },
    {
        id: 'beaupeyrat',
        name: 'Lycée Beaupeyrat',
        location: 'Limoges',
        type: 'education',
        thumbnailUrl: '/thumbnail_beaupeyrat.png',
        url: 'https://campus.beaupeyrat.fr/bts-sio/'
    },
    {
        id: 'oreplains',
        name: 'OrePlains',
        type: 'nonprofit',
        thumbnailUrl: '/thumbnail_oreplains.png',
        url: 'https://twitter.com/oreplains',
        additionalSkills: [
            {
                categoryId: 'others',
                id: 'sysadmin'
            }
        ]
    },
    {
        id: 'turgot',
        name: 'Lycée Turgot',
        location: 'Limoges',
        type: 'education',
        thumbnailUrl: '/thumbnail_turgot.svg',
        url: 'https://www.lyc-turgot.ac-limoges.fr/wp-content/uploads/2024/01/Turgot-PlaquetteGlobale-A5-2024.pdf'
    }
];