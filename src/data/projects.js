export default [
    {
        skills:  [
            {
                categoryId: 'front-end',
                id: 'vue'
            }
        ],
        id: 'portfolio',
        startDate: '2023-04-27',
        name: {
            'en': `My portfolio`,
            'fr': `Mon portfolio`
        },
        url: 'https://git.kaki87.net/KaKi87/portfolio'
    },
    {
        skills: [
            {
                categoryId: 'front-end',
                id: 'vue'
            },
            {
                categoryId: 'back-end',
                id: 'node'
            },
            {
                categoryId: 'databases',
                id: 'postgresql'
            }
        ],
        id: 'subtitle-search',
        startDate: '2022-11-09',
        name: {
            'en': `Subtitle Search`
        },
        url: 'https://subtitle-search.kaki87.net'
    },
    {
        skills: [
            {
                categoryId: 'front-end',
                id: 'vue'
            },
            {
                categoryId: 'back-end',
                id: 'node'
            },
            {
                categoryId: 'desktop',
                id: 'tauri'
            },
            {
                categoryId: 'others',
                id: 'scraping'
            }
        ],
        id: 'instagram-scraper',
        startDate: '2022-09-16',
        name: {
            'en': `Instagram scraper`
        },
        url: 'https://git.kaki87.net/KaKi87/scraper-instagram'
    },
    {
        skills: [
            {
                categoryId: 'desktop',
                id: 'electron'
            }
        ],
        orgId: 'goomeo',
        id: 'microsoft-graph-explorer-desktop',
        startDate: '2022-08-31',
        name: {
            'en': `Microsoft Graph Explorer Desktop`
        }
    },
    {
        skills: [
            {
                categoryId: 'back-end',
                id: 'node'
            }
        ],
        orgId: 'goomeo',
        id: 'posthook-cli',
        startDate: '2022-06-03',
        name: {
            'en': `Posthook CLI`
        },
        url: 'https://git.kaki87.net/KaKi87/posthook-cli'
    },
    /*{
        skills: [
            { id: 'front-end.vue' },
            { id: 'back-end.deno' }
        ],
        id: 'cv-vg',
        startDate: '2022-02-25',
        name: {
            'en': `cv.vg`
        }
    },*/
    {
        skills: [
            {
                categoryId: 'back-end',
                id: 'php'
            }
        ],
        id: 'pierre-grangereau',
        startDate: '2021-04-10',
        name: {
            'en': `Pierre Grangereau's portfolio`,
            'fr': `Portfolio de Pierre Grangereau`
        },
        isFeatured: true,
        thumbnailUrl: '/thumbnail_pierre-grangereau.png',
        isThumbnailLight: true,
        url: 'https://pierre.grangereau.fr'
    },
    {
        skills: [
            {
                categoryId: 'desktop',
                id: 'electron'
            }
        ],
        orgId: 'goomeo',
        id: 'aws-serverless-dashboard',
        startDate: '2021-02-22',
        name: {
            'en': `AWS Serverless Dashboard`
        }
    },
    {
        skills: [
            {
                categoryId: 'front-end',
                id: 'vue'
            },
            {
                categoryId: 'back-end',
                id: 'node'
            },
            {
                categoryId: 'databases',
                id: 'mysql'
            },
            {
                categoryId: 'tests',
                id: 'jest'
            },
            {
                categoryId: 'tests',
                id: 'cypress'
            },
            {
                categoryId: 'cloud',
                id: 'amazon-web-services'
            },
            {
                categoryId: 'api',
                id: 'microsoft'
            },
            {
                categoryId: 'api',
                id: 'google'
            },
            {
                categoryId: 'api',
                id: 'stripe'
            },
            {
                categoryId: 'tools',
                id: 'jira'
            }
        ],
        orgId: 'goomeo',
        id: 'verticalls',
        startDate: '2020-10-08',
        name: {
            'en': `Verticalls`
        },
        isFeatured: true,
        thumbnailUrl: '/thumbnail_verticalls.png'
    },
    {
        skills: [
            {
                categoryId: 'back-end',
                id: 'node'
            },
            {
                categoryId: 'tests',
                id: 'mocha'
            }
        ],
        id: 'sshception',
        startDate: '2020-06-07',
        name: {
            'en': `SSHception`
        },
        isFeatured: true,
        url: 'https://git.kaki87.net/KaKi87/sshception'
    },
    {
        skills: [
            {
                categoryId: 'front-end',
                id: 'vue'
            },
            {
                categoryId: 'back-end',
                id: 'node'
            },
            {
                categoryId: 'databases',
                id: 'mongodb'
            },
            {
                categoryId: 'cloud',
                id: 'microsoft-azure'
            }
        ],
        orgId: 'goomeo',
        educationId: 'lp',
        id: 'live-by-goomeo',
        startDate: '2019-10-22',
        endDate: '2020-09-29',
        name: {
            'en': `Live by Goomeo`
        },
        isFeatured: true,
        thumbnailUrl: '/thumbnail_live-by-goomeo.png'
    },
    {
        skills: [
            {
                categoryId: 'front-end',
                id: 'vue'
            },
            {
                categoryId: 'back-end',
                id: 'node'
            }
        ],
        id: 'play-search',
        startDate: '2018-09-29',
        name: {
            'en': `Play Search`
        },
        isFeatured: true,
        url: 'https://playsearch.kaki87.net'
    },
    {
        skills: [
            {
                categoryId: 'desktop',
                id: 'electron'
            }
        ],
        orgId: 'atr-limousin',
        educationId: 'bts',
        id: 'custom-erp',
        name: {
            'en': `Custom ERP`,
            'fr': `ERP personnalisé`
        }
    },
    {
        skills: [
            {
                categoryId: 'back-end',
                id: 'node'
            }
        ],
        educationId: 'bac',
        id: 'accident-detector',
        name: {
            'en': `Accident detector`,
            'fr': `Détecteur d'accident`
        },
        url: 'https://github.com/TeamCrash/crashbox'
    }
];