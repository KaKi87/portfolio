export const categories = [
    {
        id: 'front-end',
        label: {
            'en': `Front-end`
        },
        icon: 'bx-news'
    },
    {
        id: 'back-end',
        label: {
            'en': `Back-end`
        },
        icon: 'bx-server'
    },
    {
        id: 'full-stack',
        label: {
            'en': `Full-stack`
        },
        icon: 'ph-stack'
    },
    {
        id: 'databases',
        label: {
            'en': `Databases`,
            'fr': `Bases de données`
        },
        icon: 'bx-data'
    },
    {
        id: 'tests',
        label: {
            'en': `Tests`
        },
        icon: 'bx-test-tube'
    },
    {
        id: 'cloud',
        label: {
            'en': `Cloud`
        },
        icon: 'bx-cloud'
    },
    {
        id: 'desktop',
        label: {
            'en': `Desktop`,
            'fr': `Bureau`
        },
        icon: 'bx-window-alt'
    },
    {
        id: 'api',
        label: {
            'en': `API`
        },
        icon: 'bx-extension'
    },
    {
        id: 'tools',
        label: {
            'en': `Tools`,
            'fr': `Outils`
        },
        icon: 'bx-wrench'
    },
    {
        id: 'others',
        label: {
            'en': `Others`,
            'fr': `Autres`
        },
        icon: 'bx-extension'
    },
    {
        id: 'soft-skills',
        label: {
            'en': `Soft skills`,
            'fr': 'Savoir-être'
        },
        icon: 'bx-user'
    },
    {
        id: 'languages',
        label: {
            'en': `Languages`,
            'fr': 'Langues'
        },
        icon: 'bx-globe'
    }
];

const skills = [
    {
        categoryId: 'front-end',
        id: 'vue',
        label: {
            'en': `Vue`
        },
        isFeatured: true
    },
    {
        categoryId: 'back-end',
        id: 'node',
        label: {
            'en': `Node`
        },
        isFeatured: true
    },
    {
        categoryId: 'back-end',
        id: 'deno',
        label: {
            'en': `Deno`
        }
    },
    {
        categoryId: 'back-end',
        id: 'bun',
        label: {
            'en': `Bun`
        }
    },
    {
        categoryId: 'back-end',
        id: 'php',
        label: {
            'en': `PHP`
        }
    },
    {
        categoryId: 'full-stack',
        id: 'sveltekit',
        label: {
            'en': `SvelteKit`
        }
    },
    {
        categoryId: 'databases',
        id: 'mysql',
        label: {
            'en': `MySQL`
        },
        isFeatured: true
    },
    {
        categoryId: 'databases',
        id: 'postgresql',
        label: {
            'en': `PostgreSQL`
        }
    },
    {
        categoryId: 'databases',
        id: 'mongodb',
        label: {
            'en': `MongoDB`
        },
        isFeatured: true
    },
    {
        categoryId: 'tests',
        id: 'jest',
        label: {
            'en': `Jest`
        }
    },
    {
        categoryId: 'tests',
        id: 'mocha',
        label: {
            'en': 'Mocha'
        }
    },
    {
        categoryId: 'tests',
        id: 'cypress',
        label: {
            'en': 'Cypress'
        }
    },
    {
        categoryId: 'cloud',
        id: 'amazon-web-services',
        label: {
            'en': `Amazon Web Services`
        },
        isFeatured: true
    },
    {
        categoryId: 'cloud',
        id: 'microsoft-azure',
        label: {
            'en': `Microsoft Azure`
        }
    },
    {
        categoryId: 'desktop',
        id: 'tauri',
        label: {
            'en': `Tauri`
        }
    },
    {
        categoryId: 'desktop',
        id: 'electron',
        label: {
            'en': `Electron`
        }
    },
    {
        categoryId: 'api',
        id: 'microsoft',
        label: {
            'en': `Microsoft`
        }
    },
    {
        categoryId: 'api',
        id: 'google',
        label: {
            'en': `Google`
        }
    },
    {
        categoryId: 'api',
        id: 'stripe',
        label: {
            'en': `Stripe`
        }
    },
    {
        categoryId: 'tools',
        id: 'jetbrains',
        label: {
            'en': `JetBrains (IntelliJ, WebStorm)`
        }
    },
    {
        categoryId: 'tools',
        id: 'git',
        label: {
            'en': `Git (GitHub, GitLab\xA0CE, Gitea)`
        }
    },
    {
        categoryId: 'tools',
        id: 'sentry',
        label: {
            'en': `Sentry`
        }
    },
    {
        categoryId: 'tools',
        id: 'trello',
        label: {
            'en': `Trello`
        }
    },
    {
        categoryId: 'tools',
        id: 'jira',
        label: {
            'en': `Jira`
        },
        isFeatured: true
    },
    {
        categoryId: 'others',
        id: 'sysadmin',
        label: {
            'en': `Sysadmin`,
            'fr': 'Administration système'
        }
    },
    {
        categoryId: 'others',
        id: 'scraping',
        label: {
            'en': `Scraping (puppeteer,\xA0jsdom)`
        }
    },
    {
        categoryId: 'soft-skills',
        id: 'rigor',
        label: {
            'en': `Rigor`,
            'fr': `Rigueur`
        }
    },
    {
        categoryId: 'soft-skills',
        id: 'time-management',
        label: {
            'en': `Time management`,
            'fr': `Gestion du temps`
        }
    },
    {
        categoryId: 'soft-skills',
        id: 'critical-thinking',
        label: {
            'en': `Critical thinking`,
            'fr': `Esprit critique`
        }
    },
    {
        categoryId: 'soft-skills',
        id: 'initiative',
        label: {
            'en': `Initiative`,
            'fr': `Esprit d'initiative`
        }
    },
    {
        categoryId: 'languages',
        id: 'french',
        label: {
            'en': `French`,
            'fr': 'Français'
        }
    },
    {
        categoryId: 'languages',
        id: 'english',
        label: {
            'en': `English`,
            'fr': `Anglais`
        }
    }
];

export default skills;