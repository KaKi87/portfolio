import set from 'lodash.set';

import projects from './data/projects.js';
import skills, {
    categories as skillCategories
} from './data/skills.js';
import orgs from './data/orgs.js';
import {
    workExperience,
    volunteerExperience
} from './data/experience.js';
import education from './data/education.js';
import hobbies from './data/hobbies.js';

const messages = {
    'en': {
        'route': {
            'home': `Tiana Lemesle`,
            'resume': `Resume {'|'} Tiana Lemesle`,
            'project': ({ linked, named }) => `${linked(`projects.${named('orgOrEducationId') || 'personal'}.${named('id')}`)} | Projects | Tiana Lemesle`,
            'skill': ({ linked, named }) => `${linked(`skills.${named('id')}`)} | Skills | Tiana Lemesle`,
            'org': ({ named }) => `${orgs.find(org => org.id === named('id')).name} | Orgs | Tiana Lemesle`,
            'education': ({ linked, named }) => `${linked(`education.${named('id')}`)} | Education | Tiana Lemesle`
        },
        'period': {
            'since': `Since`
        },
        'jobTitle': `Web developer`,
        'home': {
            'description': `{age}yo, computer enthusiast`,
            'resume': `Resume`,
            'featured': `Featured`,
            'all': 'All'
        },
        'resume': {
            'label': `Resume`,
            'main': `with 3 years of professional experience, but since forever interested in computing, technology, science. I spend my personal time and my professional time creating apps with one goal in mind : be useful to my users. In the long run, I would like to continue doing so while also contributing to scientific research & innovation.`,
            'git': `Browse my 100+ open source projects at git.kaki87.net`
        },
        'projects': {
            'label': `Projects`,
            'personalProject': `Personal project`
        },
        'project': {
            'tmp': ({ linked, named }) => `I'm looking forward to writing about my experience crafting ${linked(`projects.${named('orgId') || named('educationId') || 'personal'}.${named('id')}`)} for you to read. Rain check ?`,
            'org': `Organization involved in this project`,
            'skills': `Skills involved in this project`
        },
        'skills': {
            'label': `Skills`
        },
        'skill': {
            'tmp': ({ linked, named }) => `I'm looking forward to writing about my experience learning and practicing ${linked(`skills.${named('id')}`)} for you to read. Rain check ?`,
            'projects': `Projects involving this skill`
        },
        'workExperience': {
            'label': `Work experience`
        },
        'org': {
            'tmp': ({ named }) => `I'm looking forward to writing about my experience at ${named('name')} for you to read. Rain check ?`,
            'projects': `Projects at this organization`,
            'skills': `Skills practiced at this organization`
        },
        'education': {
            'label': `Education`,
            'tmp': `I'm looking forward to writing about my education. Rain check ?`
        },
        'volunteerExperience': {
            'label': `Volunteer experience`
        },
        'hobbies': {
            'label': `Hobbies`
        },
        'experienceAndEducation': {
            'label': `Experience & education`,
            'company': `Company`,
            'nonprofit': `Non-profit org.`,
            'education': ({ linked }) => linked('education.label')
        },
        'footer': {
            'label': `© Tiana Lemesle – Open source portfolio & resume compiled on`,
            'buildTimestampFormat': `ll [a]t LTS`
        }
    },
    'fr': {
        'route': {
            'home': `Tiana Lemesle`,
            'resume': `CV {'|'} Tiana Lemesle`,
            'project': ({ linked, named }) => `${linked(`projects.${named('orgOrEducationId') || 'personal'}.${named('id')}`)} | Projets | Tiana Lemesle`,
            'skill': ({ linked, named }) => `${linked(`skills.${named('id')}`)} | Compétences | Tiana Lemesle`,
            'org': ({ named }) => `${orgs.find(org => org.id === named('id')).name} | Organisations | Tiana Lemesle`,
            'education': ({ linked, named }) => `${linked(`education.${named('id')}`)} | Formations | Tiana Lemesle`
        },
        'period': {
            'since': `Depuis`
        },
        'jobTitle': `Développeur web`,
        'home': {
            'description': `{age} ans, passionné d'informatique`,
            'resume': `CV`,
            'featured': `Mises en avant`,
            'all': `Toutes`
        },
        'resume': {
            'label': `CV`,
            'main': `avec 3 ans d'expérience, mais depuis toujours passionné d'informatique, de technologies, de sciences. Je passe mon temps personnel et mon temps professionnel à créer des applications avec un objectif en tête : être utile à mes utilisateurs. A long terme, j'aimerais continuer à faire cela tout en contribuant également à la recherche scientifique et à l'innovation.`,
            'git': `Parcourez mes 100+ projets open source sur git.kaki87.net`
        },
        'projects': {
            'label': `Réalisations`,
            'personalProject': `Projet personnel`
        },
        'project': {
            'tmp': ({ linked, named }) => `J'ai hâte de vous raconter mon expérience à créer ${linked(`projects.${named('orgId') || named('educationId') || 'personal'}.${named('id')}`)} pour que vous puissiez la lire. Une autre fois ?`,
            'org': `Entreprise impliquée dans ce projet`,
            'skills': `Compétences utilisées pour ce projet`
        },
        'skills': {
            'label': `Compétences`
        },
        'skill': {
            'tmp': ({ linked, named }) => `J'ai hâte de vous raconter mon expérience à apprendre et pratiquer ${linked(`skills.${named('id')}`)} pour que vous puissiez la lire. Une autre fois ?`,
            'projects': `Projets utilisant cette compétence`
        },
        'workExperience': {
            'label': `Expérience professionnelle`
        },
        'org': {
            'tmp': ({ named }) => `J'ai hâte de vous raconter mon expérience chez ${named('name')} pour que vous puissiez la lire. Une autre fois ?`,
            'projects': `Projets chez cette organisation`,
            'skills': `Compétences pratiquées chez cette organisation`
        },
        'education': {
            'label': `Formation`,
            'tmp': `J'ai hâte de vous raconter mon parcours de formation. Une autre fois ?`
        },
        'volunteerExperience': {
            'label': `Activité associative`
        },
        'hobbies': {
            'label': `Centres d'intérêt`
        },
        'experienceAndEducation': {
            'label': 'Expérience & formation',
            'company': `Entreprise`,
            'nonprofit': `Association`,
            'education': ({ linked }) => linked('education.label')
        },
        'footer': {
            'label': `© Tiana Lemesle – Portfolio & CV open source compilé le`,
            'buildTimestampFormat': `ll à LTS`
        }
    }
};

for(const project of projects){
    for(const [locale, value] of Object.entries(project.name)){
        set(
            messages,
            [
                locale,
                'projects',
                project.orgId || project.educationId || 'personal',
                project.id
            ],
            value
        );
    }
}

for(const skill of skills){
    for(const [locale, value] of Object.entries(skill.label)){
        set(
            messages,
            [
                locale,
                'skills',
                skill.id
            ],
            value
        );
    }
}

for(const skillCategory of skillCategories){
    for(const [locale, value] of Object.entries(skillCategory.label)){
        set(
            messages,
            [
                locale,
                'skillCategories',
                skillCategory.id
            ],
            value
        );
    }
}

for(const workExperienceItem of workExperience){
    for(const [locale, value] of Object.entries(workExperienceItem.title)){
        set(
            messages,
            [
                locale,
                'workExperience',
                workExperienceItem.id
            ],
            value
        );
        if(workExperienceItem.description){
            for(const [locale, value] of Object.entries(workExperienceItem.description)){
                set(
                    messages,
                    [
                        locale,
                        'workExperienceDescription',
                        workExperienceItem.id
                    ],
                    value
                );
            }
        }
    }
}

for(const org of orgs){
    set(messages, ['en', 'orgs', org.id], org.name);
    if(org.offices){
        for(const [officeId, office] of Object.entries(org.offices)){
            for(const [locale, value] of Object.entries(office.name)){
                set(
                    messages,
                    [
                        locale,
                        'orgOffices',
                        org.id,
                        officeId
                    ],
                    value
                );
            }
        }
    }
}

for(const educationItem of education){
    for(const [locale, value] of Object.entries(educationItem.label)){
        set(
            messages,
            [
                locale,
                'education',
                educationItem.id
            ],
            value
        );
    }
}

for(const volunteerExperienceItem of volunteerExperience){
    if(volunteerExperienceItem.description){
        for(const [locale, value] of Object.entries(volunteerExperienceItem.description)){
            set(
                messages,
                [
                    locale,
                    'volunteerExperienceDescription',
                    volunteerExperienceItem.orgId
                ],
                value
            );
        }
    }
}

for(const hobby of hobbies){
    for(const [locale, value] of Object.entries(hobby.label)){
        set(
            messages,
            [
                locale,
                'hobbies',
                hobby.id
            ],
            value
        );
    }
    if(hobby.items){
        for(const childHobby of hobby.items){
            for(const [locale, value] of Object.entries(childHobby.label)){
                set(
                    messages,
                    [
                        locale,
                        'hobbies',
                        childHobby.id
                    ],
                    value
                );
            }
        }
    }
}

export default messages;