if(process.env.NODE_ENV === 'development'){
    globalThis.__VUE_OPTIONS_API__ = true
    globalThis.__VUE_PROD_DEVTOOLS__ = false;
}

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { createI18n } from 'vue-i18n';
import dayjs from 'dayjs';
import dayjsLocalizedFormat from 'dayjs/plugin/localizedFormat';
import 'dayjs/locale/fr';

import i18nMessages from './i18nMessages.js';
import App from './components/App.vue';
import Home from './components/Home.vue';
import Resume from './components/Resume.vue';
import Project from './components/Project.vue';
import Skill from './components/Skill.vue';
import Org from './components/Org.vue';
import Education from './components/Education.vue';
import Footer from './components/Footer.vue';

import projects from './data/projects.js';
import orgs from './data/orgs.js';

const
    app = createApp(App),
    i18n = createI18n({
        locale: window.navigator.language.split('-')[0],
        fallbackLocale: 'en',
        messages: i18nMessages
    }),
    router = createRouter({
        history: createWebHistory(),
        routes: [
            // Unlocalized -> localized routes redirection
            ...[
                { name: 'home',      path: '/'              },
                { name: 'resume'                            },
                { name: 'resume.pdf'                        },
                { name: 'project',   path: '/project/:id'   },
                { name: 'skill',     path: '/skill/:id'     },
                { name: 'org',       path: '/org/:id'       },
                { name: 'education', path: '/education/:id' }
            ].map(({
                path,
                name
            }) => ({
                path: path || `/${name}`,
                redirect: to => ({
                    name,
                    params: {
                        'locale': i18n.global.locale,
                        ...to.params
                    }
                })
            })),
            // Localized routes
            {
                path: '/:locale',
                name: 'home',
                component: Home
            },
            {
                path: '/:locale/resume',
                name: 'resume',
                component: Resume
            },
            {
                path: '/:locale/resume.pdf',
                name: 'resume.pdf',
                beforeEnter: to => {
                    window.location.href = {
                        'en': '/Tiana_Lemesle_Resume.pdf',
                        'fr': '/Tiana_Lemesle_CV.pdf'
                    }[to.params.locale];
                }
            },
            {
                path: '/:locale/project/:orgOrEducationId?/:id',
                name: 'project',
                component: Project
            },
            {
                path: '/:locale/skill/:id',
                name: 'skill',
                component: Skill
            },
            {
                path: '/:locale/org/:id',
                name: 'org',
                component: Org
            },
            {
                path: '/:locale/education/:id',
                name: 'education',
                component: Education
            }
        ].map(route => ({
            ...route,
            component: undefined,
            components: {
                'default': route.component,
                'footer': Footer
            }
        })),
        scrollBehavior: (to, from, savedPosition) => savedPosition || { top: 0 }
    });

dayjs.extend(dayjsLocalizedFormat);

router.beforeEach(to => {
    i18n.global.locale = to.params.locale;
    dayjs.locale(to.params.locale);
});
router.afterEach(async (to, from, failure) => {
    if(failure) return;
    document.title = i18n.global.t(`route.${to.name}`, to.params);
});

app.use(i18n);
app.use(router);

app.config.globalProperties.dayjs = dayjs;

app.mount('.App');